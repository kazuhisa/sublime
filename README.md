# Sublime Text 3 #

Sublime Text 3の設定ファイルです。

# セットアップ方法 #
* Package/Userディレクトリに移動する(Preferences > Browse Packages...)
* git cloneする。
* [Package Control](https://sublime.wbond.net/installation) をインストールする
* Sublime Textを再起動するとPackage Controlが必要なPackageをインストールしてくれる。